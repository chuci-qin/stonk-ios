//
//  KeyCenter.swift
//  OpenVideoCall
//
//  Created by GongYuhua on 5/16/16.
//  Copyright © 2016 Agora. All rights reserved.
//


struct KeyCenter {
    static let AppId: String = "ea573cca5fe64866b8581ef7722ffba3"
    // assign token to nil if you have not enabled app certificate
    static var Token: String? = nil
}
