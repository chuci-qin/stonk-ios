## Stonk--IOS 简介
- UI:UIKit用于界面及交互方面
- 语言:开发语言为Swift,没有使用ObjectiveC
- 第三方库:声网ArgoSDK,用于项目中短视频部分


## 安装运行

- 下载最新版XCode如 Version 12.5.1
- 克隆bitbucket下的工程文件，以自己的URL为准。git clone https://kangshaojun@bitbucket.org/xavierqinn/stonk-ios.git
- 打开Xcode点击Open a project or file后，选中stonk-ios工程。或者打开工程目录下的Greatpto.xcworkspace文件即可。
- 选择模拟器，如Iphone12。然后点击运行按钮即可。


## 源码解读

源码解读是根据已经编写的代码做一些说明

### 界面部分

Main.storyboard为所有功能页面的界面部分，所以查找某个页面可以点开此文件即可。

LaunchScreen.storyboard为启动页面部分，加载启动设计可以点开些文件。

### 资源文件

Assets.xcassets所有图标资源请在些文件夹里查找即可。

### 权限设置

Info.plist打开些文件可以设置应用程序权限，如开启摄像头，麦克风，相册等，还有配置FacebookAppID等。

