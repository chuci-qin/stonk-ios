//
//  share1CollectionViewCell.swift
//  Greatpto
//
//  Created by Chuci Qin
//  Copyright © 2021 Yuease. All rights reserved.
//

import UIKit

class share1CollectionViewCell: UICollectionViewCell {
     @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
