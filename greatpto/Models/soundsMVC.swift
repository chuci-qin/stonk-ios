//
//  soundsMVC.swift
//  Greatpto
//
//  Created by Chuci Qin
//  Copyright © 2021 Yuease. All rights reserved.
//

import Foundation
struct soundsMVC {
    let id:String
    let audioURL:String
    let duration:String
    let name:String
    let description:String
    let thum:String
    let section:String
    let uploaded_by:String
    let created:String
    let favourite:String
    let publish:String
    
    
}
