//
//  pushNotiSettingMVC.swift
//  Greatpto
//
//  Created by Chuci Qin
//  Copyright © 2021 Yuease. All rights reserved.
//

import Foundation

struct pushNotiSettingMVC {
    let comments : String
    let direct_messages : String
    let likes : String
    let mentions : String
    let new_followers : String
    let video_updates : String
    let id : String
}
