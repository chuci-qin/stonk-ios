//
//  commentsMVC.swift
//  Greatpto
//
//  Created by Chuci Qin
//  Copyright © 2021 Yuease. All rights reserved.
//

import Foundation
struct commentNew {
    let id:String
    let userName:String
    let userID:String
    let comment:String
    let imgName:String
    let time:String
    let like:String
    let like_count:String
    let vidID:String
}
