//
//  notificationsMVC.swift
//  Greatpto
//
//  Created by Chuci Qin
//  Copyright © 2021 Yuease. All rights reserved.
//

import Foundation

struct notificationsMVC {
    let notificationString:String
    let id:String
    let sender_id:String
    let receiver_id:String
    let type:String
    let video_id:String
    
    let senderName:String
    let senderFirstName:String

    let receiverName:String
    let senderImg:String
}
