//
//  GetAllCoinsCollectionViewCell.swift
//  Greatpto
//
//  Created by Chuci Qin
//  Copyright © 2020 greatpto. All rights reserved.
//

import UIKit

class GetAllCoinsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgCoin: UIImageView!
    @IBOutlet weak var lblCoinName: UILabel!
    @IBOutlet weak var lblCoinNumber: UILabel!
    @IBOutlet weak var tick: UIImageView!
    
}
