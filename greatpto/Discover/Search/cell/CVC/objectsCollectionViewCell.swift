//
//  objectsCollectionViewCell.swift
//  Greatpto
//
//  Created by Chuci Qin
//  Copyright © 2021 Yuease. All rights reserved.
//

import UIKit

class objectsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bottomLineView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
}
