//
//  switchAccountTableViewCell.swift
//  Greatpto
//
//  Created by Chuci Qin
//  Copyright © 2021 Yuease. All rights reserved.
//

import UIKit

class switchAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var lblUserName:UILabel!
    
    @IBOutlet weak var lblName:UILabel!
    
    @IBOutlet weak var imgUser:UIImageView!
}
